const express = require("express");
const cors = require('cors');
const MongoClient = require("mongodb").MongoClient

const mongoUrl = 'mongodb://localhost:27017/test'
const app = express();

app.use(cors());

app.get('/',(req, res)=> {
    MongoClient.connect(mongoUrl,
    {
        useNewUrlParser: true
    },
    (err,db)=>{
        if(err){
            res.status(500).send('No se ha podido conectarme con la URL: '+mongoUrl);
        }else{
            res.send('Hector Aaron Juarez Tax Conectado a la db en la URL: '+mongoUrl);
            db.close();
        }
    });
});

app.listen(3000, () => {
 console.log("El servidor está inicializado en el puerto 3000");
});